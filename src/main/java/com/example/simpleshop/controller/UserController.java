package com.example.simpleshop.controller;

import com.example.simpleshop.model.User;
import com.example.simpleshop.model.UserDto;
import com.example.simpleshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value= {"/", "/login"}, method=RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView model = new ModelAndView();

        model.setViewName("login");
        return model;
    }

    @RequestMapping(value= {"/signUp"}, method=RequestMethod.GET)
    public ModelAndView signup() {
        ModelAndView model = new ModelAndView();
        UserDto userDto = new UserDto();
        model.addObject("userDto", userDto);
        model.setViewName("signUp");

        return model;
    }

    @RequestMapping(value= {"/signUp"}, method=RequestMethod.POST)
    public ModelAndView createUser(@Valid UserDto userDto, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        User userExists = userService.findUserByEmail(userDto.getEmail());

        if(userExists != null) {
            bindingResult.rejectValue("email", "error.user", "This email already exists!");
        }
        if(bindingResult.hasErrors()) {
            model.setViewName("signUp");
        } else {
            userService.saveUser(userDto);
            model.addObject("msg", "User has been registered successfully!");
            model.addObject("userDto", new UserDto());
            model.setViewName("signUp");
        }

        return model;
    }

//    @RequestMapping(value= {"/home"}, method=RequestMethod.GET)
//    public ModelAndView home() {
//        ModelAndView model = new ModelAndView();
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        User user = userService.findUserByEmail(auth.getName());
//
//        model.addObject("userName", user.getFirstname());
//        model.setViewName("home");
//        return model;
//    }

    @GetMapping(path = "/home")
    public String loadLoginPage() {
        return "home";
    }

    @RequestMapping(value= {"/access_denied"}, method=RequestMethod.GET)
    public ModelAndView accessDenied() {
        ModelAndView model = new ModelAndView();
        model.setViewName("access_denied");
        return model;
    }

    @GetMapping(path = "/contact")
    public String loadContactPage(){
        return "contact";
    }

    @GetMapping(path = "/settings")
    public String loadSettingPage(){
        return "settings";
    }

    @GetMapping(path = "/purchaseHistory")
    public String loadPurchaseHistory(){
        return "purchaseHistory";
    }

}
