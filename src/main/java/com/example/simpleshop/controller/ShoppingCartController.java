package com.example.simpleshop.controller;

import com.example.simpleshop.exception.NotEnoughProductsInStockException;
import com.example.simpleshop.model.User;
import com.example.simpleshop.service.EmailSender;
import com.example.simpleshop.service.ProductEntityService;
import com.example.simpleshop.service.ShoppingCartService;
import com.example.simpleshop.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.time.LocalDateTime;
import java.util.Date;

@Controller
public class ShoppingCartController {

    private final ProductEntityService productEntityService;
    private final ShoppingCartService shoppingCartService;
    private final EmailSender emailSender;
    private final TemplateEngine templateEngine;
    private final UserService userService;

    public ShoppingCartController(ProductEntityService productEntityService, ShoppingCartService shoppingCartService, EmailSender emailSender, TemplateEngine templateEngine, UserService userService) {
        this.productEntityService = productEntityService;
        this.shoppingCartService = shoppingCartService;
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
        this.userService = userService;
    }

    @GetMapping("/shoppingCart")
    public ModelAndView shoppingCart() {
        ModelAndView modelAndView = new ModelAndView("shoppingCart");
        modelAndView.addObject("products", shoppingCartService.getProductsInCart());
        modelAndView.addObject("total", shoppingCartService.getTotal());
        return modelAndView;
    }

    @GetMapping("/shoppingCart/addProduct/{productId}")
    public ModelAndView addProductToCart(@PathVariable("productId") Long productId) {
        productEntityService.findById(productId).ifPresent(shoppingCartService::addProduct);
        return shoppingCart();
    }

    @GetMapping("/shoppingCart/deleteProduct/{productId}")
    public ModelAndView deleteProductFromCart(@PathVariable("productId") Long productId){
        productEntityService.findById(productId).ifPresent(shoppingCartService::deleteProduct);
        return shoppingCart();
    }

    @GetMapping("/shoppingCart/checkout")
    public ModelAndView checkout() {

        String title = "Particular information about your purchase from" + LocalDateTime.now();

        Context context = new Context();
        context.setVariable("header", "header");
        context.setVariable("title", title);
        context.setVariable("description", "information");

//        User user = userService.findUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        String body = templateEngine.process("email", context);

        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userEmail = user.getUsername();
        User userByEmail = userService.findUserByEmail(userEmail);

        String subject = userByEmail.getFirstname() +" "+  userByEmail.getLastname() +" "+ "You Have Bought Items From Alledrogo";

        try {
            emailSender.sendEmailAfterCheckout(userEmail, subject, body);
            shoppingCartService.checkout();
        } catch (NotEnoughProductsInStockException e) {
            return shoppingCart().addObject("outOfStockMessage", e.getMessage());
        }
        return shoppingCart();
    }

}
