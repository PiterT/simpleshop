package com.example.simpleshop.controller;

import com.example.simpleshop.model.ProductDto;
import com.example.simpleshop.service.ProductEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/products")
public class ProductController {

    private final ProductEntityService productEntityService;

    public ProductController(ProductEntityService productEntityService) {
        this.productEntityService = productEntityService;
    }

    @GetMapping(path = {"/", "/all"})
    public String findAll(Model model){
        model.addAttribute("productsAll", productEntityService.findAll());
        return "productsList";
    }

    @GetMapping(path = "/addProduct")
    public String createProductForm(Model model){
        model.addAttribute("productDto", new ProductDto());
        return "addProduct";
    }

    @PostMapping(path = "/addProduct")
    public String createProduct(@ModelAttribute("productDto") ProductDto productDto, Model model){
        productEntityService.create(productDto);
        return "redirect:/products/all";
    }

    @GetMapping(path = "/{category}")
    public String findByCategory(@PathVariable String category, Model model){
        model.addAttribute("productsAll", productEntityService.findByCategory(category));
        return "productsList";
    }
}
