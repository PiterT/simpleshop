package com.example.simpleshop.service;

public interface EmailSender {
    void sendEmailAfterCheckout(String email, String subject, String content);
}
