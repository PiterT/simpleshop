package com.example.simpleshop.service;

import com.example.simpleshop.model.Product;
import com.example.simpleshop.model.ProductDto;

import java.util.List;
import java.util.Optional;

public interface ProductEntityService {
    List<Product> findAll();
    Optional<Product> findById(Long productId);
    void create(ProductDto productDto);

    List<Product> findByCategory(String category);
}
