package com.example.simpleshop.service;

import com.example.simpleshop.model.Product;
import com.example.simpleshop.model.ProductDto;
import com.example.simpleshop.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class ProductEntityServiceImpl implements ProductEntityService {


    private final ProductRepository productRepository;

    public ProductEntityServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(Long productId) {
        return productRepository.findById(productId);
    }

    @Override
    public void create(ProductDto productDto) {
        Product product = new Product();

        product.setName(productDto.getName());
        product.setUnitPrice(productDto.getUnitPrice());
        product.setDescription(productDto.getDescription());
        product.setManufacturer(productDto.getManufacturer());
        product.setCategory(productDto.getCategory());
        productRepository.save(product);
    }

    @Override
    public List<Product> findByCategory(String category) {

    return productRepository.findByCategory( category);
    }
}
