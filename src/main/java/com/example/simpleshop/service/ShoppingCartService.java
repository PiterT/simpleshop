package com.example.simpleshop.service;

import com.example.simpleshop.exception.NotEnoughProductsInStockException;
import com.example.simpleshop.model.Product;

import java.math.BigDecimal;
import java.util.Map;

public interface ShoppingCartService {

    void addProduct(Product product);

    Map<Product, Integer> getProductsInCart();
    BigDecimal getTotal();

    void checkout() throws NotEnoughProductsInStockException;

    void deleteProduct(Product product);
}
