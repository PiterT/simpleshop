package com.example.simpleshop.service;

import com.example.simpleshop.model.User;
import com.example.simpleshop.model.UserDto;

import java.util.Optional;

public interface UserService {
    User findUserByEmail(String email);

    Optional<String> saveUser(UserDto userDto);

}
