package com.example.simpleshop.service;

import com.example.simpleshop.model.Address;
import com.example.simpleshop.model.Role;
import com.example.simpleshop.model.User;
import com.example.simpleshop.model.UserDto;
import com.example.simpleshop.repository.RoleRepository;
import com.example.simpleshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    //    @Override
//    public void saveUser(UserDto user) {
//        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
//        user.setActive(1);
//        Role userRole = roleRepository.findByRole("ADMIN");
//        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
//        userRepository.save(user);
//    }
    @Override
    public Optional<String> saveUser(UserDto userDto) {
        Optional<String> errors = validateFormData(userDto);
        if (errors.isPresent()) {
            return errors;
        }
        userRepository.save(toUser(userDto));
        return Optional.empty();
    }

    private Optional<String> validateFormData(UserDto userDto) {

        if (!validPhoneNumber(userDto.getPhoneNumber())) {
            return Optional.of("Invalid phone number");
        }

        if (!validHomeNumber(userDto.getHomeNumber())) {
            return Optional.of("Invalid home number");
        }

        if (!validPostCode(userDto.getPostCode())) {
            return Optional.of("Invalid post code");
        }
        return null;
    }

    private boolean validPostCode(String postCode) {
        Pattern pattern = Pattern.compile("\\d{2}-\\d{3}");
        Matcher matcher = pattern.matcher(postCode);

        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    private boolean validHomeNumber(String homeNumber) {
        boolean isNotDigit = true;

        for (char oneChar : homeNumber.toCharArray()) {
            isNotDigit = Character.isDigit(oneChar);
        }
        return isNotDigit;
    }

    private boolean validPhoneNumber(String phoneNumber) {
        Pattern pattern = Pattern.compile("\\d{4}-\\d{7}");
        Matcher matcher = pattern.matcher(phoneNumber);

        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    private User toUser(UserDto userDto) {
        User user = new User();
        user.setFirstname(userDto.getFirstname());
        user.setLastname(userDto.getLastname());
        user.setEmail(userDto.getEmail());
        user.setAddress(createUserAddress(userDto));
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setActive(1);
        Role userRole = roleRepository.findByRole("ADMIN");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        return user;
    }

    private Address createUserAddress(UserDto userDto) {
        Address address = new Address();
        address.setCity(userDto.getCity());
        address.setCountry(userDto.getCountry());
        address.setHomeNumber(userDto.getHomeNumber());
        address.setPostCode(userDto.getPostCode());
        address.setStreet(userDto.getStreet());
        address.setPhoneNumber(userDto.getPhoneNumber());
        return address;
    }


}
