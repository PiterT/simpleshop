package com.example.simpleshop.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UserDto {

    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private String city;
    private String country;
    private String postCode;
    private String street;
    private String homeNumber;
    private String phoneNumber;
}