package com.example.simpleshop.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@Embeddable
public class Address {

    private String city;
    private String country;
    private String postCode;
    private String street;
    private String homeNumber;
    private String phoneNumber;
}