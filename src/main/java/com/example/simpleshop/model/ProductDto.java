package com.example.simpleshop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class ProductDto {
    private String name;
    private BigDecimal unitPrice;
    private String description;
    private String manufacturer;
    private String category;
}
