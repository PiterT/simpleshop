package com.example.simpleshop.model;

        import lombok.Data;
        import lombok.NoArgsConstructor;

        import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "transaction_item")
public class TransactionItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int count;

    @ManyToOne
    @JoinColumn(name = "transaction_Id")
    private Transaction transaction;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
}