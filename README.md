# Registration and Login Example with Spring Security, Spring Boot, Spring Data JPA, MySQL, JSP

## Prerequisites
- JDK 1.8 or later

## Stack
- Spring Security
- Spring Boot
- Spring Data JPA
- Maven
- JSP
- MySQL

## MySQL create database and tables
  CREATE DATABASE `simple_shop` /*!40100 DEFAULT CHARACTER SET utf8 */;
  
  DROP TABLE IF EXISTS `product`;
  
    CREATE TABLE `product` (
    `product_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `category` varchar(255) DEFAULT NULL,
    `description` varchar(255) DEFAULT NULL,
    `manufacturer` varchar(255) DEFAULT NULL,
    `name` varchar(255) DEFAULT NULL,
    `unit_price` decimal(19,2) DEFAULT NULL,
    `units_in_order` bigint(20) NOT NULL,
    `units_in_stock` bigint(20) NOT NULL,
    PRIMARY KEY (`product_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
  
 DROP TABLE IF EXISTS `role`;
 
    CREATE TABLE `role` (
      `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
      `role` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`role_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `transaction_item`;

    CREATE TABLE `transaction_item` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `count` int(11) NOT NULL,
      `product_id` bigint(20) DEFAULT NULL,
      `transaction_id` bigint(20) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `FKovhir4crv87qlgshhqbitn4et` (`product_id`),
      KEY `FKgdol08xx6bw7jrhb6gqs1yvyu` (`transaction_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `transactions`;

    CREATE TABLE `transactions` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `amount` decimal(19,2) DEFAULT NULL,
      `payment_status` varchar(255) DEFAULT NULL,
      `purchase_date` datetime DEFAULT NULL,
      `buyer_id` bigint(20) DEFAULT NULL,
      `seller_id` bigint(20) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `FKhdobwgwftsspm3dqro21gv7v2` (`buyer_id`),
      KEY `FK57hdi2aobqakvd525pjuochcw` (`seller_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;

    CREATE TABLE `user` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `active` int(11) DEFAULT NULL,
      `email` varchar(255) DEFAULT NULL,
      `firstname` varchar(255) DEFAULT NULL,
      `lastname` varchar(255) DEFAULT NULL,
      `password` varchar(255) DEFAULT NULL,
      `city` varchar(255) DEFAULT NULL,
      `country` varchar(255) DEFAULT NULL,
      `home_number` varchar(255) DEFAULT NULL,
      `phone_number` varchar(255) DEFAULT NULL,
      `post_code` varchar(255) DEFAULT NULL,
      `street` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_role`;

    CREATE TABLE `user_role` (
      `user_id` bigint(20) NOT NULL,
      `role_id` bigint(20) NOT NULL,
      PRIMARY KEY (`user_id`,`role_id`),
      KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8

      
insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(1, 'Samsung Galaxy S8', 600.0, 'Phone with 5.8 screen', 'China', 'Electronics', 5, 0);
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(2, 'Huawei P9 Lite', 850.0, 'Phone with 6.5 screen', 'China', 'Electronics', 4, 0);
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(3, 'Toyota Corolla', 800.0, 'Car', 'Japan', 'Motors', 5, 1); 
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(4, 'BMW e39', 25000.0, 'Car', 'Germany', 'Motors', 3, 0);
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(5, 'Men wrist watch', 540.0, 'Men analog watch', 'China', 'Fashion', 3, 0);
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(6, 'Women wrist watch', 850.0, 'Women analog watch', 'China', 'Fashion', 11, 0);
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(7, 'Vintage pastel landscape', 70.0, 'Vintage pastel painting abstract expressionist landscape', 'Europe', 'CollectiblesAndArt', 1, 0);
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(8, 'Led Style STAR WARS FX Lightsaber Light Saber', 9.99, 'Star Wars laser sword E7', 'China', 'CollectiblesAndArt', 21, 0); 
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(9, 'Office Chair', 42.99, 'Modern office executive hydraulic chair', 'Poland', 'HomeAndGarden', 1, 0); 
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(10, 'Cordless lawn mower', 42.99, 'Cordless Lawn Mower with batteries and charger', 'Poland', 'HomeAndGarden', 1, 0); 
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(11, 'Soccer Ball', 115.50, 'Adidas Europass Official Match Soccer Ball', 'US', 'SportingGoods', 1, 0); 
 
 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(12, 'Mountain Bike', 1850.0, 'Mountain bike - 2017 Giant Anthem X, Size: L', 'Finland', 'SportingGoods', 2, 0);

 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(13, 'Monopoly', 1850.0, 'Special game edition: The Golden Girls', 'UK', 'Toys', 5, 0);

 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(14, 'Puzzle Dragon Ball Z', 8.0, 'Puzzle Japanese Anime Dragon Ball Z (1000 Pieces)', 'Japan', 'Toys', 22, 0);

 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(15, 'DIGITAL MULTI-METER', 18.0, 'NEW CRAFTSMAN 8 FUNCTION DIGITAL MULTI-METER 34-82141 Volt Meter Ohmeter', 'US', 'BusinessAndIndustrial', 2, 0);


 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(16, 'Plasma Cutter', 388.0, 'PRIMEWELD Plasma Cutter CT520D 50 A /200 A Tig Arc Mma Welder 110/220V NEW', 'Spain', 'BusinessAndIndustrial', 2, 0);

 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(17, 'Electric Guitar', 388.0, 'NEW GROTE BRAND Electric Guitar Semi Hollow Body RED COLOR', 'Poland', 'Music', 4, 0);

 insert into product(product_id, name, unit_price, description, manufacturer,
 category, units_in_stock, units_in_order) values(18, 'Piano', 388.0, 'NEW GROTE BRAND Electric Guitar Semi Hollow Body RED COLOR', 'Poland', 'Music', 4, 0);
  
  